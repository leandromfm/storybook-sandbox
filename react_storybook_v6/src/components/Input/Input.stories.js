import React from 'react';
import Input from './Input';

export default {
  title: 'Form/Input',
  component: Input,
};

export const Small = () => <Input className='small' placeholder='small size' />;
export const Medium = () => (
  <Input className='medium' placeholder='medium size' />
);
export const Large = () => <Input className='large' placeholder='large size' />;

Small.storyName = 'Small Input';
